﻿namespace SkillBuilder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.opt_meta = new System.Windows.Forms.Label();
            this.opt_current = new System.Windows.Forms.Label();
            this.btn_init = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // opt_meta
            // 
            this.opt_meta.AutoSize = true;
            this.opt_meta.Location = new System.Drawing.Point(12, 9);
            this.opt_meta.Name = "opt_meta";
            this.opt_meta.Size = new System.Drawing.Size(35, 13);
            this.opt_meta.TabIndex = 0;
            this.opt_meta.Text = "label1";
            // 
            // opt_current
            // 
            this.opt_current.AutoSize = true;
            this.opt_current.Location = new System.Drawing.Point(15, 36);
            this.opt_current.Name = "opt_current";
            this.opt_current.Size = new System.Drawing.Size(35, 13);
            this.opt_current.TabIndex = 1;
            this.opt_current.Text = "label2";
            // 
            // btn_init
            // 
            this.btn_init.Location = new System.Drawing.Point(109, 9);
            this.btn_init.Name = "btn_init";
            this.btn_init.Size = new System.Drawing.Size(75, 23);
            this.btn_init.TabIndex = 2;
            this.btn_init.Text = "button1";
            this.btn_init.UseVisualStyleBackColor = true;
            this.btn_init.Click += new System.EventHandler(this.btn_init_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_init);
            this.Controls.Add(this.opt_current);
            this.Controls.Add(this.opt_meta);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label opt_meta;
        private System.Windows.Forms.Label opt_current;
        private System.Windows.Forms.Button btn_init;
    }
}

