﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

    class Pomodoro
    {

        /*
         * PASOS PARA USO DE LA CLASE POMODORO.
         *      1. DEFINIR UN OBJETO POMODORO EN LA CLASE DEL FORM.
         *      2. AGREGAR UN OBJETO DE TIPO TIMER EN EL FORM.
         *      3. (OPCIONAL) DEFINIR DOS LABEL EN EL FORM. UNO PARA LA META ACTUAL, Y OTRO PARA EL TIEMPO ACTUAL.
         *      4. (OPCIONAL) DEFINIR UN BOTON EN EL FORM. AGREGAR EN EL onClick() DEL BOTON LA LINEA -> (NOMBRE DEL OBJETO POMODORO).INIT_POMODORO(LABEL_META, LABEL_CURRENT, TIMER);
         *      5. AGREGAR EN EL METODO onTick() DEL TIMER -> (NOMBRE DEL OBJETO POMODORO).onTimerTick(LABEL_META, LABEL_CURRENT);
         */

        private Queue<TimeSpan> sequence = new Queue<TimeSpan>();
        private TimeSpan _currentMeta = new TimeSpan(0,0,0);
        private TimeSpan _currentTime = new TimeSpan(0,0,0);
        private string[] placeholders = new[] {"00:00:00", "00:25:00"};

        public Queue<TimeSpan> Sequence
        {
            get => sequence;
            set => sequence = value;
        }

        public TimeSpan currentMeta => _currentMeta;

        public TimeSpan currentTime => _currentTime;
        
        public Pomodoro()
        {
            sequence.Enqueue(new TimeSpan(0,25,0));
            sequence.Enqueue(new TimeSpan(0,5,0));
            sequence.Enqueue(new TimeSpan(0,25,0));
            sequence.Enqueue(new TimeSpan(0,5,0));
            sequence.Enqueue(new TimeSpan(0,10,0));
        }

        public TimeSpan deque()
        {
            TimeSpan temp_TimeSpan = sequence.Dequeue();
            sequence.Enqueue(temp_TimeSpan);
            return temp_TimeSpan;
        }

        public void step()
        {
            if (_currentTime >= _currentMeta)
            {
                _currentTime = new TimeSpan();
                _currentMeta = deque();
            }

            _currentTime = currentTime.Add(new TimeSpan(0, 1, 0));
        }

        public string[] placeHolder()
        {
            return placeholders;
        }

        public void onTimerTick(System.Windows.Forms.Label meta, System.Windows.Forms.Label current)
        {
            step();
            outputTimes(meta, current);
        }

        public void outputTimes(System.Windows.Forms.Label meta, System.Windows.Forms.Label current)
        {
            meta.Text = currentMeta.ToString();
            current.Text = currentTime.ToString();
        }

        public void outputPlaceholders(System.Windows.Forms.Label meta, System.Windows.Forms.Label current)
        {
            current.Text = placeholders[0];
            meta.Text = placeholders[1];
        }

        public void init_pomodoro(System.Windows.Forms.Label meta, System.Windows.Forms.Label current, System.Windows.Forms.Timer timer)
        {
            outputPlaceholders(meta, current);
            timer.Enabled = true;
            timer.Start();
        }
    }

