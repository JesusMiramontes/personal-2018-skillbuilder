﻿using System;
using System.Windows.Forms;

namespace SkillBuilder
{
    public partial class Form1 : Form
    {
        Pomodoro pomodoro = new Pomodoro();

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            pomodoro.onTimerTick(opt_meta, opt_current);
        }

        private void btn_init_Click(object sender, EventArgs e)
        {
            pomodoro.init_pomodoro(opt_meta, opt_current, timer1);
        }
    }
}
