﻿using System.Data.SQLite;

namespace _100daysofcode
{
    class Database
    {
        public Database()
        {
            // Creating a databes
            SQLiteConnection.CreateFile("db.sqlite");

            // Declaring a connection
            SQLiteConnection connection;

            // Initializing the connection
            connection = new SQLiteConnection("Data Source= db.sqlite;");

            // Openning the connection
            connection.Open();

            // Creating a table
            string sql = "create table highscores (name varchar(20), score int)";

            // Runnning sql command
            SQLiteCommand command = new SQLiteCommand(sql, connection);

            // Execute command
            command.ExecuteNonQuery();
        }

        
    }
}
